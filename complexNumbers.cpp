#include <iostream>
#include "complexNumbers.h"

cNum::cNum(double a, double b)
{
    x=a;
    y=b;
}
cNum cNum::operator+(const cNum n1)
{
    return cNum(x+n1.x, y+n1.y);
}
cNum cNum::operator+=(const cNum n1)
{
    x+=n1.x;
    y+=n1.y;
    return *this;
}
cNum cNum::operator-(const cNum n1)
{
    return cNum(x-n1.x, y-n1.y);
}
cNum cNum::operator-=(const cNum n1)
{
    x-=n1.x;
    y-=n1.y;
    return *this;
}
bool cNum::operator==(const cNum n)
{
    if(x==n.x && y==n.y)
        return true;
    else
        return false;
}
bool cNum::operator!=(const cNum n)
{
    if(x==n.x && y==n.y)
        return false;
    else
        return true;
}
std::ostream & operator<< (std::ostream &exit, const cNum n)
{
    if (n.x !=0)
    {
        exit << n.x << " ";
        if (n.y > 0)
            exit << "+ ";
        if (n.y !=0)
            {
            if (n.y!=1 && n.y!=-1)
                exit << n.y;
            else if (n.y==-1)
                exit << "- ";
            exit << "i";
            }
    }
    else if (n.y!=0)
    {
        exit << "    ";
        if (n.y !=0)
            {
            if (n.y!=1 && n.y!=-1)
                exit << n.y;
            else if (n.y==-1)
                exit << "-";
            exit << "i";
            }
    }
    else
        exit << "0";
    return exit;
}

cSet::cSet()
{
    head = NULL;
}

cSet::~cSet()
{
	cNum *current = head;
	cNum *next;
    while( current )
        {
			next = current->next;
            delete current;
			current = next;
        }
	head = NULL;
}
cNum* cSet::getPt(int n)const
{
    if(!head)
        return NULL;
    cNum *temp = head;
	for(int i=0; i<n; ++i)
	{
		if( i == n-1)
			return temp;
		else if(temp->next)
			temp = temp->next;
		else
            return NULL;
	}
	return NULL;
}
void cSet::add(double x, double y)
{
 	cNum *newNum = new cNum(x,y);
 	newNum->next = NULL;
 	if(head == NULL)
 	{
 	    head = newNum;
		return;
 	}
 	else
	{
    	 cNum *temp = head;
    	 while(temp)
    	{
    	    if(*temp==*newNum)
    	    {
    	        delete newNum;
    	        return;
    	    }
    	    else if(temp->next == NULL)
    	    {
    	        temp->next=newNum;
    	        return;
    	    }
    	    temp = temp->next;
    	}
	}
}
void cSet::rmove(double x, double y)
{
    if(!head)
    {
        return;
    }
    else
    {
        cNum *temp = head;
		cNum *prev = NULL;
        int i = 1;
        while (temp)
        {
            if(x == temp->x && y == temp->y)
            {
                if(temp == head)
                    head = head->next;
                else
                {
                    //prev = getPt(i-1);
                    prev->next = temp->next;
               }
                delete temp;
                return;
            }
			++i;
			prev = temp;
            temp = temp->next;
        }
    }
	return;
}
cSet cSet::operator +(const cSet &s) const&
{
    cSet sTemp;
    cNum *temp = s.head;
    while (temp)
    {
        sTemp.add(temp->x,temp->y);
        temp = temp->next;

    }
    temp = head;
    while (temp)
    {
        sTemp.add(temp->x,temp->y);
        temp = temp->next;
    }
    return sTemp;
}

cSet& cSet::operator +=(const cSet &s)
{

    cNum *temp = s.head;
    while (temp)
    {
        add(temp->x,temp->y);
        temp = temp->next;
    }
    return *this;
}
cSet cSet::operator *(const cSet &s) const&
{
    cSet sTemp;
    cNum *temp = s.head;
    cNum *temp2 = head;
    while (temp)
    {
        while(temp2)
        {
            if(*temp==*temp2)
                sTemp.add(temp->x,temp->y);
            temp2 = temp2->next;
        }
        temp = temp->next;
    }
    return sTemp;
}
cSet& cSet::operator *=(const cSet &s)
{
    cNum *temp = head;
    while (temp)
    {
		cNum *temp2 = s.head;
        bool flag = false;
        while(temp2)
        {
            if(*temp==*temp2)
                flag = true;
            temp2 = temp2->next;
        }
		cNum *help = temp;
		temp = temp->next;
        if(!flag)
            rmove(help->x,help->y);

    }
    return *this;
}
std::ostream & operator<< (std::ostream &out, const cSet &s)
{
    int i=1;
	cNum *temp = s.head;

    if(s.head == NULL)
    {
        out << "Ten zbior jest pusty" << std::endl;
        return out;
    }
	while(temp != NULL) {
	    out << i << ". " << *temp << std::endl;
	  	temp = temp->next;
	    ++i;
	 	}
    return out;
}
